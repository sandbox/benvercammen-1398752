The goal of this module is to keep track of all "Reference ID's" along with their (matching) UUID's.

This can be useful in the following cases:
 1. on importing content via node_export, the "node_export_relation" module falls short
 	-> it can repair UUID-NID links only for those nodes available in the "batch import" and already existing nodes
 		- first import a batch containing nodes with a reference to non-existing nodes
 		- then import a batch containing the nodes to which the reference was made
 		=> this isn't handled by the node_export module...
 2. on creating nodes via Feeds, once again we can not "repair" the NID-UUID links if one feed depends on another
 

So the method we apply is to check if a repair is needed, every time a "reference field" is created (not updated).

For the created node with a reference field, we give precedence to the UUID value: 
 - If we can't find a node with the given UUID, we unset the value of the NID (the UUID value is kept).
 - If we find a node with the given UUID, we update the NID field (if necessary).
 - If no UUID is given, we look up the UUID matching the NID. (mostly on CREATE or on "incomplete imports")

Once created, we look up all "node references" that have an empty ("") NID, and a matching UUID.
We can then update the NID to that of the node matching the set UUID.

This way, we are able to maintain UUID references instead of NID references, so we don't have to worry about it while importing nodes.

Or at least that's what we think we can......





!! TODO :: field_revision_field_... ook nog updaten!