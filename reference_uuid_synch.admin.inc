<?php

require_once 'reference_uuid_synch.install';

/**
 * @file
 * Administration functions for the reference_uuid_synch module.
 */

/**
 * Menu callback: Reference UUID Synch
 * 
 * @param $form
 * @param $form_state
 */
function reference_uuid_synch_admin_form($form, $form_state) {
	$form = array();
	$form['synch']['#type'] = 'fieldset';
	$form['synch']['#title'] = t('Synchronization');
	$form['synch']['#description'] = 'Overview of all UNRESOLVED UUID references. In some cases it is best to just remove them.';
	// TODO :: nog meer uitleg hierbij...
	
	// Via the callback method, let's look at all referenced items...
	$schema = drupal_get_complete_schema();
	reference_uuid_synch_schema_alter($schema, '_list_referenced_items');
	
	$html = '<table border="1">';
	$html .= '<tr><th>Referenced UUID</th><th>From</th><th>Bundle</th><th>with ID</th><th>column</th><th>table</th></tr>';
	
	$unresolved = $_REQUEST['unresolved_uuids'];
	foreach ($unresolved as $unresolved_reference) {
		$html .= '<tr><td>';
		$html .= $unresolved_reference['uuid'];
		$html .= '</td><td>';
		$html .= $unresolved_reference['entity_type'];
		$html .= '</td><td>';
		$html .= $unresolved_reference['bundle'];
		$html .= '</td><td>';
		$html .= $unresolved_reference['entity_id'];
		$html .= '</td><td>';
		$html .= $unresolved_reference['col_name'];
		$html .= '</td><td>';
		$html .= $unresolved_reference['table_name'];
		$html .= '</td></tr>';
	}
	$html .= '</table>';
	$form['synch']['#description'] .= $html;
	
	$form['synch']['submit']['#type'] = 'submit';
	$form['synch']['submit']['#value'] = 'Verwijderen!';
	$form['synch']['submit']['#submit'] = array();
	$form['synch']['submit']['#submit'][] = '_remove_unresolved_references';
	
	return system_settings_form($form);
}


/**
 * Callback submit function that removes ALL unresolved references.
 * 
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function _remove_unresolved_references($form, $form_state) {
	// TODO :: eigenlijk mag da niet he, toch niet als het required fields zijn...
	// TODO :: dus ?????
	// TODO /: dan zou de volledige node moeten verwijderd worden???
	/*
	 * Possible cause:
	 * 	- node met die UUID is niet mee ge-exporteerd / importeerd
	 */
	// TODO :: mss ook gewoon op "inactive" (not published) zetten dan??? ma dan nog...
	
}









/**
 * List all referenced items that don't have either a UUID or Reference ID.
 * 
 * @param $ext				One of "_nid", "_uid", "_fid", "_tid", ...
 * @param $name				The name of the table
 * @param $table			The modified table
 * @param $uuid_colname		The name of the added UUID column
 * @param $rid_colname		The name of the original Reference ID column
 */
function _list_referenced_items($ext, $name, $table, $uuid_colname, $rid_colname) {
	
	// Unresolved items...
	$query = db_select($name, 'f');
	$query->fields('f');
	$query->condition($rid_colname, null);
	$unresolved_references = $query->execute();
	$_REQUEST['unresolved_uuids'] = array();
   	foreach ($unresolved_references as $index => $value) {
   		// TODO :: unresolved UUID's...
   		$item['uuid'] = $value->$uuid_colname;
   		$item['col_name'] = $uuid_colname;
   		$item['table_name'] = $name;
   		$item['entity_id'] = $value->entity_id;
   		$item['entity_type'] = $value->entity_type;
   		$item['bundle'] = $value->bundle;
   		// TODO :: en bundle??
   		$_REQUEST['unresolved_uuids'][] = $item;
//		drupal_set_message('Unresolved UUID: ' . $value->$uuid_colname . ' in table "' . $name . '" for node "' . $value->entity_id . '"');
//		echo(var_dump($index));
//		die(var_dump($value));
   	}
}